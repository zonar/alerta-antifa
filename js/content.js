var Antifa = Antifa || {};
(function () {
    "use strict";
    Antifa.content = {
        prefix:"alerta-antifa-",

        /**
         * Initialise obj
         *
         * @Event : Wait a message from background.js
         */
        init:function() {
            var self = this;
            this.name = this.getMessage('extensionName');

            chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
                if (msg.text === 'load_popup') {
                    self.createPopup();
                    sendResponse(document.all[0].outerHTML);
                }
            });
        },
        /**
         * Create a large popup
         *
         * @param domContent
         */
        createPopup:function() {
            var self = this;
            var hover = this.createDiv('hover');
            this.addElementToBody(hover);

            var popup  = this.createDiv('popup');

            var close = this.createDiv('close', 'X');
            popup.appendChild(close);

            var img = this.addImage(chrome.extension.getURL("img/icon.png"), this.name);
            popup.appendChild(img);

            var text = this.appendMessage(this.getMessage('contentPopupMessage'));
            popup.appendChild(text);

            this.addElementToBody(popup);

            document.getElementById(this.prefix + 'close').addEventListener('click', function() {
                self.onClickClose()
            }, false);
        },

        /**
         * Get message in _locales
         */
        getMessage: function(key) {
            return chrome.i18n.getMessage(key)
        },

        /**
         * Create a DIV element
         *
         * @param id
         * @param text
         * @returns {HTMLElement}
         */
        createDiv: function(id, text) {
            var div = document.createElement("div");
            div.setAttribute('id', this.prefix + id);
            if(null != text) {
                var node = document.createTextNode(text);
                div.appendChild(node);
            }
            return div;
        },

        /**
         * Create an IMG element
         *
         * @param text
         * @returns {HTMLElement}
         */
        addImage:function(src, alt) {
            var img = document.createElement('img');
            img.src= src;
            if (alt != null) img.alt= alt;
            return img;
        },

        /**
         * Create P element
         *
         * @param text
         * @returns {HTMLElement}
         */
        appendMessage: function(text) {
            var el = document.createElement("p");
            var node = document.createTextNode(text);
            el.appendChild(node);
            return el;
        },

        /**
         * Add element to BODY element
         *
         * @param element
         */
        addElementToBody:function(element) {
            var body = document.getElementsByTagName("body")[0];
            body.appendChild(element);
        },

        /**
         * On click to close remove component
         *
         * @event click
         */
        onClickClose:function() {
            this.removeElement('popup');
            this.removeElement('hover');
        },

        /**
         * Remove element
         *
         * @param id
         */
        removeElement: function(id) {
            var el = document.getElementById(this.prefix + id);
            el.parentNode.removeChild(el);
        }
    };
    Antifa.content.init();
})();