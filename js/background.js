var Antifa = Antifa || {};

(function () {
    "use strict";

    Antifa.background = {
        history: [],
        alerta: [],

        /**
         * Initialize obj
         *
         * Send a message to content to load popup
         */
        init: function() {
            var self = this;
            this.name = this.getMessage('extensionName');

            this.loadData('js/alerta.json');
            chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
                self.checkDomain(self, tabId, changeInfo, tab);
            });
        },

        /**
         * Check if current domain is an Fascist website
         *
         *  - update history if is OK
         */
        checkDomain: function(scope, tabId, changeInfo, tab) {
            chrome.tabs.query({}, function(tabs) {
                if (tab.status == 'complete') {
                    var domain = scope.getDomain(tab.url);
                    if(scope.alerta.indexOf(domain) != -1) {
                        if(scope.history.indexOf(domain) == -1) {
                            scope.history.push(domain);
                            /*
                             * @Todo check if notification is available
                             *  - Possible or not ??
                             *  - is OK launch notif
                             *  - is KO lauch popup
                             */
                            scope.notification();
                            chrome.tabs.sendMessage(tabId, {text: 'load_popup'}, scope.onAfterCreatePopup);
                        }
                    }
                }
            });
        },

        /**
         * Load json data
         *
         * @param path
         *
         * @TODO : Complete error
         */
        loadData: function(path) {
            var self = this;
            var request  = new XMLHttpRequest();
            request.open("GET", chrome.extension.getURL(path), true);
            request.onload  = function (data) {
                if (request.status >= 200 && request.status < 400) {
                    var d = JSON.parse(request.responseText);
                    self.alerta = d.data;
                } else {
                    /**
                     * @TODO Data not load
                     */
                }
            };
            request.onerror = function() {
                /**
                 * @TODO  There was a connection error of some sort
                 */
            };
            request.send();
        },

        /**
         * Extract the domaine
         */
        getDomain: function(url) {
            return url.match(/^[\w-]+:\/*\[?([\w\.:-]+)\]?(?::\d+)?/)[1]
        },

        /**
         * Get message in _locales
         */
        getMessage: function(key) {
            return chrome.i18n.getMessage(key)
        },

        /**
         * Send chrome notification to user
         *
         * User must activate notification
         */
        notification: function() {
            return new Notification(this.name, {
                'icon' : 'img/icon_128.png',
                'body' : this.getMessage('backgroundNotificationMessage')
            });
        },

        /**
         * Clear history
         */
        clearHistory: function() {
            this.history = [];
        },

        /**
         * Method call when content.js respond
         *
         * @TODO add console ??
         */
        onAfterCreatePopup:function() {}
    };

    Antifa.background.init();
})();

